# Stampede-ClusterGenomes

## Overview

**Stampede-ClusterGenomes** is a perl script for clustering genomes.

## Installation

[Update] ClusterGenomes no longer assumes the user is working on Stampede and has access to its modules. Nucmer (compiled for 64-bit linux, part of the MUMmer package) is now included.

* [Perl 5.16](https://www.perl.org/get.html) (should be standard with most distros)
* [MUMmer 3.0](http://mummer.sourceforge.net/) (technically, nucmer)

The user will need to install the clustergenomes.pl script and nucmer executable to their path. Both are found in the bin directory of this repo.

### Inputs

The only input is a FASTA-formatted file with the sequences to cluster (**-f**).

### General Options

**-c**: Percent of the sequence covered. Default: 80.

**-i**: Percent identity to use for clustering. Default: 95.

### Execution

The wrapper.template and test.sh files outline how the script(s) are run. First the system's path is extended to include the bin directory (allowing the system to find nucmer and the perl script from anywhere on the system). Then the test files are set, along with the coverage and identities desired. Finally, the system executes the perl script (that internally will use nucmer) passing the above mentioned options.

Essentially this runs: 

```
Cluster_genomes.pl -f fastafile.fasta -c coverage -i indentity
```

At the end of the script there's a redirect for the stdout and stderr to capture information that would *normally* print to the screen.

### Authors

* Simon Roux (script author)
* Ben Bolduc (Cyverse integration)

For a full list of contributions, see the Contributors file.