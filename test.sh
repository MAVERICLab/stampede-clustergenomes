#!/bin/bash

# Agave automatically writes these scheduler
# directives when you submit a job but we have to
# do it by hand when writing our test

#SBATCH -p development
#SBATCH -t 00:30:00
#SBATCH -n 16
#SBATCH -A iPlant-Collabs
#SBATCH -J test-clustergen
#SBATCH -o test-clustergen.o%j
#SBATCH -e test-clustergen.e%j

module load intel/15.0.2 mvapich2/2.1 python/2.7.9

# Extend paths
export PATH=$PATH:"$PWD/bin"

# Inputs
fasta="${PWD}/test/Viral_contigs.fasta"

# Args
ARGS=""

# Args
coverage=80
identity=95

ARGS="${ARGS} -f ${fasta} -c ${coverage} -i ${identity}"

# Run the actual program
echo Cluster_genomes.py ${ARGS}
Cluster_genomes.py ${ARGS}

# Cleanup
#rm -rf bin/